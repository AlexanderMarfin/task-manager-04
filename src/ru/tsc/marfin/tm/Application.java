package ru.tsc.marfin.tm;

import ru.tsc.marfin.tm.constant.TerminalConst;

public final class Application {

    public static void main(final String[] args) {
        process(args);
    }

    public static void process(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        switch (arg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showError(String arg) {
        System.err.printf("Error! This argument `%s` is not supported \n", arg);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexander Marfin");
        System.out.println("E-mail: amarfin@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("1.2.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info. \n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application version. \n", TerminalConst.VERSION);
        System.out.printf("%s - Show terminal commands. \n", TerminalConst.HELP);
    }

}
